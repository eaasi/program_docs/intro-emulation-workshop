---
layout: page
title: "Contributor Code of Conduct"
---
EaaSI staff and contributors strongly encourage use of the Recurse Center’s Social Rules:

- No well-actually’s
- No feigned surprise
- No backseat driving
- No subtle -isms
    
These are social rules that EaaSI workshop leaders strive to live in; they do not constitute an officially
enforced Code of Conduct. The latter will depend on the venue and/or professional organization hosting
us for our events.

EaaSI workshop leaders WILL inform all event/workshop/lesson attendees of available and relevant
policies at the beginning of the event.
