---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---

> ## About this lesson
>
> This lesson was designed by Claire Fox and Ethan Gates, and originally performed as an in-person workshop in New York City at the Metropolitian New York Library Council (METRO) in January 2020.
>
> **Claire Fox** is an A/V archivist and graduate of NYU's Moving Image Archiving and Preservation program ('20).
>
> **Ethan Gates** is a Software Preservation Analyst for Yale University Library. He primarily works on user support and emulation configuration for the EaaSI program.
{: .callout}

<br>
<br>
Emulation is commonly associated with video game consoles and questionable internet download sites. 
But the same technology can be a powerful tool for overcoming legacy software and hardware dependencies 
hidden in digital collections.

What if you get your data off of old floppies, CD-ROMs, or hard drives, but still can’t access the files 
because they depend on decades-old software? What if the files lose data or don’t look right when 
converted or opened in a modern format? Emulation can help by recreating the whole computing 
environment your data was originally intended for.

Attendees will be walked through the basic concepts, components and process of setting up emulators 
to authentically present and interact with legacy data. We will also introduce ongoing cutting-edge 
efforts by the Emulation-as-a-Service Infrastructure (EaaSI) program and the Software Preservation 
Network to make emulation more accessible to all.

**This tutorial will involve using the command line!** If you have never used a terminal/command line application before, we strongly encourage completing a tutorial or two on that topic *first* to make sure you are comfortable executing and troubleshooting the setup and steps necessary here. A few resources we recommend:
- [Really Friendly Command Line Intro](https://hellowebbooks.com/learn-command-line/), Tracy Osborn
- [Introduction to the Bash Command Line](https://programminghistorian.org/en/lessons/intro-to-bash), Programming Historian
- [An Introduction to Using the Command Line Interface (CLI) to Work with Files and Directories](https://www.weareavp.com/wp-content/uploads/2015/09/CLI_MacOS_Tutorial.pdf), Bertram Lyons
- [How to use the Windows command line (DOS)](https://www.computerhope.com/issues/chusedos.htm), Computer Hope

<!-- this is an html comment -->

{% comment %} This is a comment in Liquid {% endcomment %}

> ## Prerequisites
>
- a laptop with approximately 5 GB of free storage
- a bit of command line comfort (see above)
- install QEMU and Mini vMac (see [Setup](setup.html))
- download sample files provided (see [Setup](setup.html))
{: .prereq}

{% include links.md %}
