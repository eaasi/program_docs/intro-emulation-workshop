---
title: Setup
---

This tutorial has been tested and can be completed using MacOS, Windows, or Linux. In order to complete the tutorial using the provided example files, you will need approximately 5 GB of free space of storage (and we'd recommend something more like 10 GB if possible, to really explore in some of the activities!)

You will need to download and/or install two open-source emulators to complete the workshop activities: **QEMU**, a generic PC emulator, and **Mini vMac**, which
emulates a [Macintosh Plus](https://en.wikipedia.org/wiki/Macintosh_Plus).

### Installing QEMU
QEMU is a command-line application, and most easily installed using package managers. Its project web site contains [instructions for download](https://www.qemu.org/download/) depending on your OS. **Please be certain to use QEMU 4.2.x or higher**. Certain legacy operating systems, including Windows 95, may not work properly with
other versions. A few additional notes:

**MacOS Users**  
QEMU is best installed on MacOS using [Homebrew](https://brew.sh/). If you do not already have Homebrew on your system,
follow Homebrew's instructions for installation *first*, then QEMU's instructions for MacOS installation.

(Homebrew *should* install QEMU 4.2.x by default)

**Windows Users**  
The QEMU project does not officially maintain Windows versions, but links to [Windows installers provided](https://qemu.weilnetz.de/w64/) by their
contributor Stefan Weil.

The makers of this workshop also highly recommend [Chocolatey](https://chocolatey.org/), which is essentially the same
as Homebrew, for Windows. If you install Chocolatey first, QEMU can easily be installed by typing `choco install qemu` in a Command
Prompt or PowerShell window.

(Chocolatey *should* install QEMU 4.2.x by default, otherwise be sure to choose a manual installer that lines up with this version)

**Linux Users**  
Linux users should be able to easily install QEMU with their distribution's package manager. Just be careful to check that your
distro's repositories install version 4.2.x by default (Ubuntu users, for example, will need to be on 20.04 LTS or above). If your
package manager only offers version 4.0.x or lower, please manually download and build from the source code offered on QEMU's website.


### Installing Mini vMac
Mini vMac is a stand-alone application that should require no further setup or installation once downloaded and unzipped.

You can visit the official project [Download](https://www.gryphel.com/c/minivmac/dnld_std.html) page and select the appropriate package for your system.

Once downloaded and unzipped, we recommend placing the application file (.app, .exe) in the "Mini vMac" folder included with the downloads below.


### Downloading Sample Content

The sample files to complete the exercises in this tutorial can be downloaded [here](https://drive.google.com/drive/folders/1yZkYpsD23V33Qh5cOY-FYhrRcnxMA6De?usp=sharing).
You should download **all files**, including "Mini vMac" and "Windows95" folders, and the "Software Metadata Scavenger Hunt - EaaSI Sandbox" spreadsheet. Put the downloads wherever convenient on your computer.


{% include links.md %}
