---
title: "Emulation and Virtualization Applications"
teaching: 25
exercises: 45
questions:
- "What does emulation look like in practice?"
- "What are unique challenges to Mac emulation?"
objectives:
- "Boot a Windows 95 virtual machine in QEMU"
- "Boot a Mac System 6 virtual machine in Mini vMac"
- "Load and run a disk image in emulation"
keypoints:
- "Running an emulator can require a challenging combination of troubleshooting legacy software/operating systems
and the emulator itself."
- "Mac hardware is more unique than generic PC hardware, so emulation is more challenging and requires extra
files to recreate particular Mac models. These extra files are called 'ROMS'."
---

## Create a Windows 95 Virtual Machine

Using the following steps, we will now demonstrate emulation in practice by creating a Windows 95
virtual machine using the emulation/virtualization application QEMU (Quick EMUlator).

> **NOTE**: Again, QEMU is primarily a terminal or "command line" application. If you have never used the command line before,
we recommend reading or completing a command line tutorial *before* attempting this section of the workshop to
ensure that you are comfortable with basic conventions and principles of using command line software.

Please make sure you have followed all the steps and instructions in Setup before proceeding.

1. Open a command line terminal (Terminal in MacOS/Linux, Command Prompt in Windows)
<br>

2. Change directory into the “Windows95” folder you downloaded during Setup (e.g. Downloads -> Windows95)
  e.g. type `“$ cd /path/to/Windows95`
  (cd, space, drag-and drop the “Windows95” folder on to the terminal screen, hit Enter)
<br>

3. Create a blank hard disk image:
`$ qemu-img create -f qcow2 WindowsHDD.img 2G`
- This command calls QEMU's program for manipulating and creating disk images (`qemu-img`) and directs it to create a blank disk image with a maximum size of 2 GB and using the "qcow2" file format.
- The `qcow2` format is a disk image format unique to QEMU optimized for emulation and virtual machines. It allows you to create a disk image that goes **up to** 2 GB without
actually taking up that much space on your host storage!
<br>

4. Start installation by typing:
`$ qemu-system-i386 -cpu pentium -m 256 -vga cirrus -soundhw sb16 -net nic,model=pcnet -hda WindowsHDD.img -fda Windows95_boot_disk.img -cdrom Win95_installer.iso -boot a`
- `qemu-system-i386` calls QEMU's generic 32-bit x86 PC emulator (ideal for mid-90s Windows!)
- `-cpu pentium`
- `-m 256` assigns 256 MB of RAM to the virtual machine
- `-vga cirrus`
- `-soundhw sb16`
- `-net nic,model=pcnet`
- `-net user`
- `-hda WindowsHDD.img`
- `-fda Windows_95_boot_disk.img`
- `-cdrom Win95_installer.iso`
- `-boot a` directs the virtual machine to boot from the floppy drive
<br>
The Windows 95 installer was distributed on a CD-ROM but was not bootable - so if you're starting from scratch/a blank hard drive like we are, you needed a boot floppy in addition to the Windows 95 installer to get set up.
<br>

5. A QEMU window should open and you will boot to an MS-DOS prompt: Select or type "1" to boot your virtual machine with CD-ROM support.
<img src="../assets/img/Screenshot_1.png"/>

6. When MS-DOS loads, you should receive a prompt: `A:\> `. Type `fdisk` then hit Enter.
   <img src="../assets/img/Screenshot_2.png"/>
7. DOS will ask if you want to "enable large disk support". This is obscurely asking how you would like to format the blank hard disk drive: FAT16 or FAT32. Type "Y" and Enter to format your hard drive with the NTFS file system and allow for formatting drives over 2GB.
<img src="../assets/img/Screenshot_3.png"/>

8. In the fdisk (Fixed Disk Setup Program) menu, Enter choice "1" to create a DOS partition on your hard drive.
<img src="../assets/img/Screenshot_4.png"/>

9. Select "1" again to create a Primary DOS Partition.
<img src="../assets/img/Screenshot_5.png"/>

10. Finally, select "Y" to use your entire 2 GB hard drive for the Primary DOS Partition.
<img src="../assets/img/Screenshot_6.png"/>

11. Hit "Esc" to exit fdisk, then close the QEMU window entirely to "turn off" your virtual machine.
<img src="../assets/img/Screenshot_7.png"/>

12. Back in your terminal, hit the Up Arrow key and restart QEMU again (with all the same flags/settings). Again, select "1" to boot with CD-ROM support.

13. At the DOS `A:\> ` prompt, type `format c:` to complete the process of partitioning and formatting your hard drive. Proceed with the formatting "Y" when prompted.
<img src="../assets/img/Screenshot_8.png"/>

14. When formatting is complete, label the hard drive volume as desired (e.g. "WINDOWS")

15. At the `A:\> ` prompt, type "D:" to change directories to the CD-ROM drive.
<img src="../assets/img/Screenshot_9.png"/>

16. At the `D:\> ` prompt, type "SETUP" to launch the Windows 95 installation program. (Allow the installer to run a ScanDisk check of your "hard drive" first)
<img src="../assets/img/Screenshot_10.png"/>

17. The Windows 95 installer instructions should at this point be relatively self-explanatory, and most default options can be accepted.
<img src="../assets/img/Screenshot_11.png"/>
  - At the "Setup Options" screen, choose the "Typical" setting
  <img src="../assets/img/Screenshot_13.png"/>
  - At the Certificate of Authenticity screen, enter in the serial number provided in the "serial.txt" file included in the downloads for this tutorial.
  <img src="../assets/img/Screenshot_14.png"/>
  - At the "User Information" screen, enter anything for "Name" and Company".
  <img src="../assets/img/Screenshot_15.png"/>
  - At the "Analyzing Your Computer" screen, check the boxes for both "Network Adapter" and "Sound, MIDI or Video Capture Card" so that the installer looks for those pieces of hardware.
  <img src="../assets/img/Screenshot_16.png"/>
  - At the "Windows Components" screen, choose to the install the most common components.
  <img src="../assets/img/Screenshot_17.png"/>
  - At the "Startup Disk" screen, select **"No, I do not want a startup disk"!!!**. You already have a startup/boot disk - we started this tutorial with it!
  <img src="../assets/img/Screenshot_18.png"/>
<br>

18. When files are done copying and installing you will be prompted to reboot the computer, and see a warning that there is still a floppy disk in your floppy drive.
<img src="../assets/img/Screenshot_20.png"/>
  - Make sure QEMU is your active window and type "CTRL-ALT-2" to    access the "QEMU Console" (which allows you to perform some actions that you would for a physical machine, like eject a floppy disk!)
  - type "eject floppy0"
  <img src="../assets/img/Screenshot_21.png"/>
  - type "CTRL-ALT-1" to return to your virtual machine
  - reboot as prompted
<br>

19. After rebooting, you will be asked to set up a few operating system settings.
  - You can select whatever Date & Time/Time Zone you would like, but we recommend *unchecking* the "Automatically adjust clock for daylight saving changes" box. This will avoid a number of annoying pop-up warnings.
  <img src="../assets/img/Screenshot_22.png"/>
  - "Cancel" to avoid configuring a printer. (Unless you have a spare Apple LaserWriter around?)
  - Reboot again.
<br>

20. At this point, you have a working Windows 95 installation. **Congratulations!!!** But let's set up a few more things to get the most you can out of Windows.
<img src="../assets/img/Screenshot_24.png"/>

21. Avoid configuring Internet Explorer. It's not going to do well with the internet anyway.

22. To get proper video and audio, go to the Windows menu -> Settings -> Control Panel.
  - Select the "System" menu
  - Under "Device Manager", there will be a question mark next to "Plug and Play BIOS". QEMU does not properly emulate the hardware necessary to play nice with this part of Windows 95, but we can fix it. (It will get a bit scary. Don't panic!)
  <img src="../assets/img/Screenshot_29.png"/>
<br>

23. Select "Plug and Play BIOS" and then click on "Properties".

24. On the "Driver" tab, click on "Update Driver".
<img src="../assets/img/Screenshot_30.png"/>

25. In the Update Device Driver Wizard, select "No, select driver from list"
<img src="../assets/img/Screenshot_31.png"/>

26. In the list of options presented, select "PCI bus", then "Finish". You will be prompted to reboot.
<img src="../assets/img/Screenshot_32.png"/>

27. When you reboot, **a bunch of stuff will happen**. This is basically Windows 95 suddenly detecting and configuring lots of pieces of hardware that it was unable to see before. Continue accepting whatever default options are presented to you (e.g. to overwrite or keep certain files) and allow the system to reboot several times.

28. At some point the installer will properly detect your network adapter and ask you to configure your network. You must fill in a user/computer name and Workgroup, but you can put anything.
<img src="../assets/img/Screenshot_33.png"/>

29. Eventually, you should reboot back to the Windows 95 desktop. To log on Windows will ask for your user name (the user/computer name you selected a minute ago) and a password. You may invent any password you like here, or just leave the field blank to have an empty (no) password.

30. If you didn't hear audio before...you should hear the beautiful Microsoft Sound now!

31. At the desktop, return to the Control Panel (Start -> Settings -> Control Panel) and select the "Display" menu.
<img src="../assets/img/Screenshot_25.png"/>
  - In the "Settings" tab, select "High Color (16-bit)" under "Color Palette" and boost "Desktop area" to 1024 by 768 pixels.
  <img src="../assets/img/Screenshot_27.png"/>
  - Select "Apply" - Windows will give you a warning about changing the desktop resolution on the fly, but you can safely apply these settings without restarting.
<br>
31. Congrats, you now have the latest in cutting-edge 1995 Soundblaster and SVGA audiovisual technology!

32. Try out a CD-ROM: enter the QEMU Console (by typing CTRL-ALT-2)
  - Enter `change ide1-cd0 MarbleDrop.iso` to insert the demo CD-ROM included with the Windows 95 downloads for this tutorial.
  - Hit CTRL-ALT-1 to return to the Windows 95 interface. "Marble Drop" should appear in the CD-ROM (D:) drive!
<br>
33. To properly shut down your Windows 95 machine, go to the Start menu -> Shut down. The QEMU window should close automatically when you shut down the operating system and return you to your terminal.

34. If you ever want to run your Windows 95 machine again, make sure you are in the "Windows 95" directory and run:
`$ qemu-system-i386 -m 256 -vga cirrus -soundhw sb16 -net nic,model=pcnet -usb -hda WindowsHDD.img`. You no longer need the `-boot`, `-fda` or `-cdrom` flags, although you can use the latter two if you have another floppy or CD-ROM image you would like to try with your Win95 virtual machine! (Try browsing the options at the [Internet Archive](https://archive.org/details/software) or [WinWorld](https://winworldpc.com/home) - have fun!)


## Create a Mac OS 6 Virtual Machine

- Open the “Mini vMac” folder.

- Make sure your Mini vMac app (.app/.exe) is in the **same folder** as the “vMac.ROM” file.
<img src="../assets/img/Screenshot_38.png"/>

> ## What's the deal with the "ROM" file?
>
> Mac emulation presents additional challenges compared to general PC/x86 systems (e.g. our Windows 95 VM). Apple operating systems and emulators are often tied to very particular Mac machines/models rather than being able to run on a "generic" emulated processor. For instance, the Mini vMac emulator is designed rather specifically to emulate a Macintosh Plus and handle the operating systems available for that model, whereas BasiliskII can potentially recreate a slightly broader range of Motorola 68K-based Macs (Quadras, Performas, etc.)
>
> In order to recreate a particular Mac machine, several of these emulators (including Mini vMac) require a "ROM" (read-only memory) file, containing firmware dumped from an original machine's motherboard.
>
> The legality of distributing ROM files is considered murky enough that many emulator projects and even "abandonware" software download sites will not post them. Along with the other downloads used in this lesson, we are making a Macintosh Plus ROM file freely available with the stipulation that it be used in a non-profit educational or digital preservation context (i.e. to access legacy digital files that would otherwise be abandoned and not served by Apple)
{: .callout}

<br>

- Double-click “Mini vMac”(.app/.exe). You should hear a startup beep, and see a flashing floppy
<img src="../assets/img/Screenshot_39.png"/>

- In “Apple Mac OS 6.0.3” folder, drag and drop “System Tools.img” on to the Mini vMac window
<img src="../assets/img/mini_vmac.gif"/>

- Drag and drop other disk images from “Mini vMac” folder into your emulated Macintosh Plus!

{% include links.md %}
