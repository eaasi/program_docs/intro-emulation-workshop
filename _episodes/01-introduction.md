---
title: "Introduction and Icebreaker"
teaching: 5
exercises: 10
questions:
- "Who are we? Who are you?"
objectives:
- "Introduce instructors and attendees to each other"
keypoints:
- "Keep your earliest software/computer you ever used in mind. We'll try to circle back to it later!"
---

### Some background on your course designers:

**Ethan Gates** is the Software Preservation Analyst for Yale University Library, currently working primarily
on the "Scaling Emulation and Software Preservation Infrastructure" (EaaSI, for short) project. He started,
like many people, using emulators to access nebulously legal Nintendo ports on his laptop, and has been
investigating using the technology in archival and media studies contexts since at least [2016](https://patchbay.tech/2016/04/11/powerpc-mac-emulation/). 

**Claire Fox** is a media archivist and 2020 graduate of NYU's M.A. program in Moving Image Archiving
and Preservation. She interned at YUL working on the EaaSI project in [summer 2019](https://www.softwarepreservationnetwork.org/blog/who-preserves-eaasi-in-pursuit-of-an-eaasi-preservation-package/).

### A few questions to reflect/get us started:

- Have you ever used an emulator before? What for?
- What's your *earliest* memory of using a computer? What software/application were you using?
- Have you ever had trouble opening an old file? What did you do to fix or get around the problem?

{% include links.md %}
