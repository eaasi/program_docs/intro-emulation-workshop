---
title: "Emulation as a Service"
teaching: 20
exercises: 20
questions:
- "How can we scale emulation for institutions and collections?"
objectives:
- "Load an environment in the EaaSI sandbox"
- "Consider extra-technical implications of emulation for digital preservation (Fair Use, software
  preservation and description, shared collections and infrastructure)"
keypoints:
- "Emulation-as-a-Service offers a technical platform to simplify the use of emulation in broad-ranging digital collections."
- "Standardized description of software and hardware is a difficult but essential task for collaborative recovery and preservation efforts."
- "The doctrine of 'Fair Use' is a flexible legal tool that can potentially be used to legally many software preservation activities - but be mindful that context matters, and that these recommendations are as of yet unproven in court."
---

## Emulation-as-a-Service

Emulation-as-a-Service, or EaaS, is a web-based platform originally developed by the [bwFLA](http://eaas.uni-freiburg.de/) team at the University of Freiburg. It allows users to access multiple emulators (or multiple versions of the same emulator) from a single browser interface - greatly simplifying the redundant configuration work necessary to run emulators at scale (many different operating systems, many different combinations of hardware, etc.)

The [EaaSI project](https://eaasi.info/), which designed and sponsored this lesson, also maintains a [sandbox version](https://sandbox.eaasi.cloud) of EaaS to demonstrate the experience and capabilities of running emulation in a browser. It is populated with open source software (e.g. Linux, BSD, and OpenSolaris systems).

> ## EaaS Reflection
>
>- In the EaaSI Open Source Software Sandbox, click on the tab in the navigation menu labeled "Environments."  
>- Select an option from the menu presented - any Environment, it's up to you.  
>- Click on the "Choose Action" button for your selected Environment, then select "Run Environment".  
>- An emulation session should start in your browser, and you'll see something like this:
> <p align='center'><img src="../assets/img/sandbox.png"/></p>
> Please be patient - depending on the strength of your connection and the health of EaaSI's servers, Environments may load slowly.
>
> ## Questions
>- What is the experience of using an emulator in a browser?
>- How does it compare to the experience of running QEMU or Mini vMac?
{: .challenge}

## Software Description

Scaling the use of emulation and tools like EaaS across many cultural heritage organizations and archives will require organized and agreed-upon ways to talk about computer software and hardware. Otherwise, we will launch many redundant and unnecessary efforts: if Museum X emulates a copy of "Microsoft Office" and University Archive emulates a copy of "Microsoft Office 2003 Small Business Edition", how can we be sure they are or are not in fact running the same program?

However, establishing a standard for description of software and emulated hardware is a difficult task, not least of which because software programmers and distributors have used such a wide variety of methods, practices, and style guides to describe their own work and requirements.

Let's try a brief activity to identify and demonstrate some of the challenges involved with software description and metadata.

> ## Cataloging Software
>
>1. Return to the EaaSI Open Source Software Sandbox. Select and run another Environment of your choice.
>2. Inside the emulated operating system, explore the menus and select one or two programs to try to "catalog" - a web browser, an image editor, a word processor, a music player, it doesn't matter. Any two applications will do - any of them could potentially be necessary for study and description!
>3. In the Setup folder downloaded for this lesson, open the "Software Metadata Scavenger Hunt - EaaSI Sandbox" spreadsheet. This spreadsheet offers a minimal schema and fields/columns for describing software applications, including an example item to guide you in this activity.
>4. To the best of your ability, use the spreadsheet to "catalog" the two applications you selected, one application per row.
>5. When finished, consider the reflection/discussion questions below.
>
> ## Questions
>- Was it easy to catalog your selected programs? Difficult?
>- Do you think the challenges in describing your software were unique to the program you chose? Or inherent to the schema and fields provided?
>- Try to identify three factors that would make this task easier - think big! Technology, resources, knowledge, staffing, collaborative efforts, etc.
{: .challenge} 

## Copyright and Intellectual Property Concerns

Though legacy software, like the examples we have emulated during this lesson, is an essential tool for accessing recovered and historical data, emulation and software preservation have not yet been widely incorporated into preservation workflows. A persistent concern, visible even in the hobbyist and enthusiast communities that push emulators forward, is legal uncertainty. Copying or modifying digital works, including software, can technically constitute "reproduction" or "distribution", putting core preservation activities potentially under the purview of U.S. copyright and intellectual property law.

While preservation activities for analog materials have been, largely, explicitly blessed by the U.S. copyright system (e.g. that digitizing library copies of VHS tapes is alowed under Section 108 of the Copyright Act, or that the scanning and discovery activities behind HathiTrust and Google Books are a transformative "Fair Use", benefiting the common good rather than infringing on publishers' business and copyright), there has been no clear guidance from Congress or the courts as to equivalent activities for software preservation.

Thankfully, recent work from the Association of Research Libraries, the Software Preservation Network, and the Harvard CyberLaw Clinic have resulted in a series of empowering guidance and documents that provide a firmer legal footing for preservation organizations and practitioners to discuss, assess, and undertake risks in this gray area.

This section is less a structured activity and more of a homework assignment. It is highly recommended that any individual seeking to use emulators and legacy software to recover, access, or otherwise preserve digital works read these two documents. Though it is especially relevant and geared toward those who work at cultural heritage or non-profit institutions (gallaries, libraries, archives, museums), the Fair Use doctrine is an extremely broad and flexible legal principle (though context sensitive), with potential implications and application to preservation activities at many levels.

- ["Code of Best Practices in Fair Use for Software Preservation"](https://www.arl.org/wp-content/uploads/2018/09/2019.2.28-software-preservation-code-revised.pdf)
- ["A Preservationist's Guide to the DMCA Exemption for Software Preservation"](http://softwarepn.webmasters21.com/wp-content/uploads/2019/01/2018-12_DMCAchecklist_updated_12132018.pdf)

## Further Resources

For more information on the EaaSI program of work:  
[https://www.softwarepreservationnetwork.org/projects/emulation-as-a-service-infrastructure](https://www.softwarepreservationnetwork.org/projects/emulation-as-a-service-infrastructure)

[https://eaasi.info](https://eaasi.info)

For everything you need to deep dive further on using emulators of all sorts:  
[https://github.com/eg-tech/emulation-resources](https://github.com/eg-tech/emulation-resources)


{% include links.md %}
